import React from "react";

// creation of context object

const UserContext = React.createContext();

// the Provider component allows other components to consume/use the context object and supply the necessary information needed.
export const UserProvider = UserContext.Provider;

export default UserContext;