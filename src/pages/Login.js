import {Form, Button} from "react-bootstrap"
import {useState, useEffect, useContext} from "react"
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from 'sweetalert2';


export default function Login() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  
  const [isActive, setIsActive] = useState('');

    console.log(email);
    console.log(password);

    // Allows us to consume the User Context/Data and its properties for validation
    const {user, setUser} = useContext(UserContext)


    function loginUser(e){
      e.preventDefault();


      // set the email of the authenticated user in the local storage
      /*
      Syntax:
          localStorage.setItem("propertyName", value)
      */
      localStorage.setItem("email", email)

      // Set the global user state to have properties obtained from local storage.
      // This will pass the data to the UserContext which is ready to use from all different endpoints/pages
      setUser({
        email: localStorage.getItem('email')
      })

      setEmail("");
      setPassword("");

      alert("You are now Logged in!")
    }


    useEffect(()=>{
      if(email !== "" && password !== ""){
        setIsActive(true)
      }else{
        setIsActive(false)
      }
    },[email, password])



  return (
    // Conditional renderin
    // LOGIC - if there is a user logged in in the web application, endpoint or "/login" should not be accessible. The user should be navigated to courses tab instead.

    (user.id !== null) ?
    <Navigate to="/courses"/>
    :
    <Form onSubmit={loginUser}>
      <Form.Group className="mb-3" controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Enter your Email Address"
          value={email} onChange={e=>setEmail(e.target.value)} required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Enter your Password"
        value={password} onChange={e=>setPassword(e.target.value)} required/>
      </Form.Group>

    {isActive ?
      <Button variant="primary" type="submit" id="loginBtn">
        Submit
      </Button>
      :
      <Button variant="secondary" type="submit" id="loginBtn" disabled>
        Submit
      </Button>
    }
    </Form>
  );
}