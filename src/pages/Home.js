import Banner from "../components/Banner";
// import CourseCard from "../components/CourseCard";
import Highlights from "../components/Highlights";
import { Container } from "react-bootstrap";


export default function Home(){

  const data = {
    title:"Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere!",
    destination: "/Courses",
    label: "Enroll Now!"
  }

  return(
    <>
    <Banner bannerProp={data}/>
    <Highlights/>
    <Container>
    {/* <CourseCard/> */}
    </Container>
    </>
  )
}