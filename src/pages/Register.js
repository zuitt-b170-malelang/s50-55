// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Form, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';


/*
  Mini Activity - add input fields for firstName, lastName, and mobileNo
*/

//Define state hooks for all input fields and an "isActive" state for conditional rendering of the submit btn

export default function Register() {
    //create state hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email,setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    const {user, setUser} = useContext(UserContext)

    //create a state to determine whether the submit button is enabled or not

    const [isActive, setIsActive] = useState('');

    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password1);
    console.log(password2);


    setUser({
      email: localStorage.getItem('email')
    })
    // Function to simulate user registration
    function registerUser(e){

      // prevents page redirection via form submission
      e.preventDefault();

      setFirstName("");
      setLastName("");
      setEmail("");
      setMobileNo("");
      setPassword1("");
      setPassword2("");

      alert("Thank you for registering!")

    }

    /*
        Two Way Binding
        -it is done so that we can assure that we can save the input into our states as we type into the input elements. This is done so what we don't have to save it just before we submit

        e.target - current element where the event happens
        e.target.value - current value of the element where the event happened


    */

  useEffect(()=>{

      // Enable the submit button if:
      // All the fields are populated.
      // both passwords match.

    if((firstName !=="" && lastName !== "" && mobileNo.length === 11 && email !=="" && password1 !=="" && password2 !=="")&&(password1===password2)){
      setIsActive(true)
    }else{
      setIsActive(false)
    }

  },[firstName,lastName,email,mobileNo,password1,password2])




  return (
    (user.id !== null) ?
    <Navigate to="/courses"/>
    :
    <Form onSubmit={(e)=>registerUser(e)}>
      <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter First Name"
        value={firstName}
        onChange={e=>setFirstName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Last Name"
        value={lastName}
        onChange={e=>setLastName(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
        type="email"
        placeholder="Enter email"
        value={email}
        onChange={e=>setEmail(e.target.value)}
        required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
        type="text"
        placeholder="Enter Mobile Number"
        value={mobileNo}
        onChange={e=>setMobileNo(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Password"
        value={password1}
        onChange={e=>setPassword1(e.target.value)}
        required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Password</Form.Label>
        <Form.Control
        type="password"
        placeholder="Verify Password"
        value={password2}
        onChange={e=>setPassword2(e.target.value)}
        required/>
      </Form.Group>


        {/*Conditional rendering - submit button will be active based on the isActive state*/}
        { isActive ?      
            <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
          :
            <Button variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
        }

    </Form>
  );
}
