// import {Fragment} from "react";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom' 
import { Container } from "react-bootstrap";
import './App.css';
import AppNavBar from "./components/AppNavBar";
import Home from "./pages/Home";
import Courses from "./pages/Courses"
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from './pages/Logout';
import React from 'react';
import Error from './pages/Error';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import UserContext from './UserContext';


function App() {

 //used to store user info and will be used for validating if a user is logged in in the app or not 
const [user, setUser] = useState({
  id: null,
  isAdmin: null
})

const unsetUser = () => {
  localStorage.clear();
}

useEffect(()=>{
  fetch('http://localhost:4000/users/details', {
    headers:{
      Authorization: `Bearer ${localstorage.getItem('token')}`
    }
  })
  .then(res=>res.json())
  .then(data=>{
    console.log(data);
  })

  if(typeof data._id)


})



  return (   

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
            <AppNavBar/>
              <Container>
                <Routes>   
                  <Route path='/' element={<Home/>}/>  
                  <Route path='/courses' element={<Courses/>}/>
                  <Route path='/register' element={<Register/>}/>
                  <Route path='/login' element={<Login/>}/>
                  <Route path='/logout' element={<Logout/>}/>
                  <Route path="*" element={<Error/>}/>
                </Routes>
              </Container>
          </Router>
    </UserProvider>

   


  );
  
}

export default App;
